# AdGuard Exporter
Prometheus exporter for metrics of AdGuard Home service

## How to install
### Docker Compose
```dockerfile
version: '3'
services:
  dahuavto2mqtt:
    image: "registry.gitlab.com/elad.bar/adguard-exporter:latest"
    container_name: "adguard-exporter"
    hostname: "adguard-exporter"
    restart: "unless-stopped"
    environment:
        - SERVER_PORT=9877
        - ADGUARD_HOSTNAME=localhost
        - ADGUARD_PORT=8080
        - ADGUARD_PROTOCOL=http
        - ADGUARD_USERNAME=user
        - ADGUARD_PASSWORD=pass
        - INTERVAL=30
        - DEBUG=False      
```

### Environment Variables
| Variable         | Default   | Description                                                       |
|------------------|-----------|-------------------------------------------------------------------|
| SERVER_PORT      | 9877      | Port for the Prometheus collector                                 |
| ADGUARD_HOSTNAME | localhost | AdGuard instance hostname or IP                                   |
| ADGUARD_PORT     | 8080      | AdGuard instance port                                             |
| ADGUARD_PROTOCOL | http      | AdGuard instance protocol (http/https)                            |
| ADGUARD_USERNAME | user      | AdGuard instance username                                         |
| ADGUARD_PASSWORD | pass      | AdGuard instance password                                         |
| INTERVAL         | 30        | Interval in seconds between collecting data from AdGuard instance |
| DEBUG            | false     | Enable debug log messages                                         |

## Metrics
```
# HELP adguard_top_queried_domains Top queried domains
# TYPE adguard_top_queried_domains gauge
adguard_top_queried_domains{domain="full.domin.name",instance="localhost"} 1.0

# HELP adguard_top_clients Top clients
# TYPE adguard_top_clients gauge
adguard_top_clients{client="fqdn.hostname",instance="localhost"} 1.0

# HELP adguard_top_blocked_domains Top blocked domains
# TYPE adguard_top_blocked_domains gauge
adguard_top_blocked_domains{domain="full.domin.name",instance="localhost"} 1.0

# HELP adguard_top_upstreams_responses Top upstreams
# TYPE adguard_top_upstreams_responses gauge
adguard_top_upstreams_responses{instance="localhost",upstream="8.8.8.8"} 5.0

# HELP adguard_top_upstreams_avg_time Average upstream response time
# TYPE adguard_top_upstreams_avg_time gauge
adguard_top_upstreams_avg_time{instance="localhost",upstream="8.8.8.8"} 0.001

# HELP adguard_num_dns_queries DNS Queries
# TYPE adguard_num_dns_queries gauge
adguard_num_dns_queries{instance="localhost"} 0.0

# HELP adguard_num_blocked_filtering Blocked by Filters
# TYPE adguard_num_blocked_filtering gauge
adguard_num_blocked_filtering{instance="localhost"} 0.0

# HELP adguard_num_replaced_safebrowsing Blocked malware/phishing
# TYPE adguard_num_replaced_safebrowsing gauge
adguard_num_replaced_safebrowsing{instance="localhost"} 0.0

# HELP adguard_num_replaced_safesearch Enforced safe search
# TYPE adguard_num_replaced_safesearch gauge
adguard_num_replaced_safesearch{instance="localhost"} 0.0

# HELP adguard_num_replaced_parental Blocked adult websites
# TYPE adguard_num_replaced_parental gauge
adguard_num_replaced_parental{instance="localhost"} 0.0

# HELP adguard_avg_processing_time Average processing time
# TYPE adguard_avg_processing_time gauge
adguard_avg_processing_time{instance="localhost"} 0.001
```