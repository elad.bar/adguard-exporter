import asyncio
import json
import logging
import os
import sys
from prometheus_client import start_http_server, Gauge, CollectorRegistry
from aiohttp import ClientSession, BasicAuth

# Logging
DEBUG = str(os.environ.get('DEBUG', False)).lower() == str(True).lower()
log_level = logging.DEBUG if DEBUG else logging.INFO

root = logging.getLogger()
root.setLevel(log_level)

stream_handler = logging.StreamHandler(sys.stdout)
stream_handler.setLevel(log_level)
formatter = logging.Formatter('%(asctime)s %(levelname)s %(name)s %(message)s')
stream_handler.setFormatter(formatter)
root.addHandler(stream_handler)

_LOGGER = logging.getLogger(__name__)


class AdguardExporter:
    def __init__(self) -> None:
        _LOGGER.debug("Initializing Adguard Exporter")

        self.registry = CollectorRegistry()
        self.exporter_port = int(os.environ.get("SERVER_PORT", "9877"))
        self.hostname = os.environ.get('ADGUARD_HOSTNAME', "localhost")
        self.port = int(os.environ.get('ADGUARD_PORT', "8080"))
        self.protocol = os.environ.get('ADGUARD_PROTOCOL', "http")
        self.username = os.environ.get('ADGUARD_USERNAME')
        self.password = os.environ.get('ADGUARD_PASSWORD')
        self.interval = int(os.environ.get('INTERVAL', "30"))

        self.adguard_api_url = f"{self.protocol}://{self.hostname}:{self.port}"

        self.session: ClientSession | None = None

        self.auth: BasicAuth | None = None

        if self.username and self.password:
            self.auth = BasicAuth(self.username, self.password)

        _LOGGER.info(
            "Adguard Exporter configuration loaded, "
            f"Port: {self.exporter_port}, "
            f"AdGuard URL: {self.adguard_api_url}"
        )

        self.metrics: dict | None = None
        self.metrics_data: dict[str, Gauge] = {}

    def stats_cleanup(self, stats):
        _LOGGER.debug("Cleanup outdated metrics")
        metrics_keys = self.metrics.keys()

        for metric_key in metrics_keys:
            metric_config = self.metrics[metric_key]
            tags = metric_config.get("tags", [])

            if len(tags) > 1:
                metric_data = metric_config.get("data", {})

                stats_data = stats.get(metric_key)

                for metric_data_key in metric_data.keys():
                    if metric_data_key not in stats_data:
                        stats_data[metric_data_key] = 0

    async def get_latest_stats(self) -> dict:
        result = {}
        try:
            _LOGGER.debug("Collecting latest statistics from server")

            url = f"{self.adguard_api_url}/control/stats"
            async with self.session.get(url, verify_ssl=False) as stats_response:
                stats_response.raise_for_status()

                stats_data = await stats_response.json()

                # Flat JSON from array of objects to object
                for metric_key in stats_data.keys():
                    metric_config = self.metrics.get(metric_key)

                    if metric_config is None:
                        continue

                    tags = metric_config.get("tags", [])
                    stats_metric_data = stats_data.get(metric_key)

                    if len(tags) > 1:
                        metric_data = {}

                        for stats_metric_data_item in stats_metric_data:
                            metric_data.update(stats_metric_data_item)

                        result[metric_key] = metric_data
                    else:
                        result[metric_key] = stats_metric_data

        except Exception as ex:
            _LOGGER.error(f"Failed to get statistics from server, Error: {ex}")

        return result

    async def adjust_client_name(self, stats):
        try:
            _LOGGER.debug("Matching top client IPs to hostnames")

            clients = stats.get("top_clients", {})
            client_ip_list = list(clients.keys())
            client_ip_req_parts = []

            for i in range(len(client_ip_list)):
                client_ip_req_parts.append(f"ip{i}={client_ip_list[i]}")

            request_query = "&".join(client_ip_req_parts)

            url = f"{self.adguard_api_url}/control/clients/find?{request_query}"

            async with self.session.get(url, verify_ssl=False) as client_response:
                client_response.raise_for_status()

                client_data = await client_response.json()
                client_data_items = {}
                for client_data_item in client_data:
                    client_data_items.update(client_data_item)

                for client_ip in client_ip_list:
                    client_info = client_data_items.get(client_ip, {})
                    client_name = client_info.get("name", client_ip)
                    client_stat = clients.get(client_ip)

                    if client_stat is not None:
                        clients.pop(client_ip)
                        clients[client_name] = client_stat

        except Exception as ex:
            _LOGGER.error(f"Failed to update clients from server, Error: {ex}")

    def set_metrics(self, stats):
        try:
            _LOGGER.debug("Set metrics values")

            for metric_key in stats.keys():
                metric_config = self.metrics.get(metric_key)
                gauge: Gauge = self.metrics_data.get(metric_key)
                stats_data = stats.get(metric_key)

                if metric_config is None:
                    continue

                tags = metric_config.get("tags", [])

                if len(tags) > 1:
                    metric_config["data"].update(stats_data)
                    for stats_data_key in stats_data:
                        stats_data_value = stats_data.get(stats_data_key)

                        gauge.labels(self.hostname, stats_data_key).set(stats_data_value)

                else:
                    metric_config["data"] = stats_data
                    gauge.labels(self.hostname).set(stats_data)

        except Exception as ex:
            _LOGGER.error(f"Failed to set metrics, Error: {ex}")

    async def update(self):
        latest_stats = await self.get_latest_stats()

        await self.adjust_client_name(latest_stats)
        self.stats_cleanup(latest_stats)

        self.set_metrics(latest_stats)

    async def start_collecting(self):
        start_http_server(self.exporter_port)

        async with ClientSession() as session:
            self.session = session

            while True:
                await asyncio.sleep(self.interval)

                await self.update()

    def initialize(self):
        loop = asyncio.new_event_loop()

        with open('metrics.json', 'r') as f:
            self.metrics = json.load(f)

        for metric_config_key in self.metrics.keys():
            metric_config_data = self.metrics.get(metric_config_key)
            description = metric_config_data.get("description")
            tags = metric_config_data.get("tags")

            self.metrics_data[metric_config_key] = Gauge(f"adguard_{metric_config_key}", description, tags)

            self.registry.register(self.metrics_data[metric_config_key])

        _LOGGER.info(
            f"Collecting metrics: {', '.join(list(self.metrics.keys()))}"
        )

        loop.run_until_complete(self.start_collecting())


if __name__ == "__main__":
    c = AdguardExporter()
    c.initialize()
