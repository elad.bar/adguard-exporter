FROM python:3.12-alpine
MAINTAINER Elad Bar <elad.bar@hotmail.com>

WORKDIR /app

COPY . ./

RUN apk update && \
    apk upgrade && \
    apk add curl && \
    pip install -r /app/requirements.txt

ENV SERVER_PORT=9877
ENV ADGUARD_HOSTNAME=localhost
ENV ADGUARD_PORT=8080
ENV ADGUARD_PROTOCOL=http
ENV ADGUARD_USERNAME=user
ENV ADGUARD_PASSWORD=pass
ENV INTERVAL=30
ENV DEBUG=False

RUN chmod +x /app/exporter.py

HEALTHCHECK --interval=30s --timeout=3s CMD wget --spider http://127.0.0.1:${SERVER_PORT}/metrics

ENTRYPOINT ["python3", "/app/exporter.py"]